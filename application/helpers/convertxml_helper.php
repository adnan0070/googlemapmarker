<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function convertArrayToXML($resultArray){

  $xmlDocument = new DOMDocument( "1.0", "ISO-8859-15" );
  header( "content-type: application/xml; charset=ISO-8859-15" );


  $node = $xmlDocument->createElement("markers");
  $parnode = $xmlDocument->appendChild($node);

  foreach ($resultArray as $row) {
     // print_r($row);
   //   Add to XML document node
      $node = $xmlDocument->createElement("marker");
      $newnode = $parnode->appendChild($node);
      $newnode->setAttribute("id", $row['id']);
      $newnode->setAttribute("name", $row['name']);
      $newnode->setAttribute("address", $row['address']);
      $newnode->setAttribute("lat", $row['lat']);
      $newnode->setAttribute("lng", $row['lng']);
      $newnode->setAttribute("type", $row['type']);
    }

    echo   $xmlDocument->saveXML();

  }
?>