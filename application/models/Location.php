<?php 
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Location extends CI_Model {

	public function __construct()
	{ 
		parent::__construct(); 


	}   

	public function getAllLocations()
	{
		$this->db->select('*');
		$this->db->from('markers');
		$result=$this->db->get();
		return $result->result_array();
	}

}