<!DOCTYPE html >
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <title>Using MySQL and PHP with Google Maps</title>


    <script type='text/javascript' src="<?php echo base_url(); ?>assets/javascript/maphandler.js"></script>
      <script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>

      <script src="https://code.jquery.com/jquery-1.10.2.js"></script>


    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>

  <body>
    <div id="map"></div>

    <script>
      var customLabel = {
        restaurant: {
          label: 'R'
        },
        bar: {
          label: 'B'
        }
      };

      // declare map and infoWindow variables so that we access them through page
      var map;
      var infoWindow;
        
        function updateMap(){

         // First time when map will initialized we will take a reference of map.
         map = new google.maps.Map(document.getElementById('map'), {
           center: new google.maps.LatLng(-33.863276, 151.207977),
            zoom: 12
         });

           infoWindow = new google.maps.InfoWindow;
           getMarkerData();
        }

        function getMarkerData(){

          //call method from maphandler.js and it will return xml file

          downloadUrl('<?php echo base_url("/Home/getXMLFile");?>', function(data) {

            addMarkersToMapView(map, infoWindow, data);

          });
        }

        var timeInterval = setInterval(function(){ updateTimer() }, 10000);
        
        function updateTimer(){

            getMarkerData();
        }

    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC0d1KbRUNTP1LjoA6EOwZisBSVeXN_aSI&callback&callback=updateMap">
    </script>
  </body>
</html>