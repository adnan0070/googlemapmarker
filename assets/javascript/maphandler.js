function downloadUrl(url, callback) {

  var request = window.ActiveXObject ?
  new ActiveXObject('Microsoft.XMLHTTP') :
  new XMLHttpRequest;


  request.onreadystatechange = function() {
    if (request.readyState == 4) {
      request.onreadystatechange = doNothing;
      callback(request, request.status);
    }
  };

  request.open('GET', url, true);
  request.setRequestHeader("Content-type", "text/xml");
  request.send(null);
}

function doNothing() {}

function addMarkersToMapView(map, infoWindow, data){

  var xml = data.responseXML;
  var markers = xml.documentElement.getElementsByTagName('marker');

  Array.prototype.forEach.call(markers, function(markerElement) {

    var id = markerElement.getAttribute('id');
    var name = markerElement.getAttribute('name');
    var address = markerElement.getAttribute('address');
    var type = markerElement.getAttribute('type');
    var point = new google.maps.LatLng(
      parseFloat(markerElement.getAttribute('lat')),
      parseFloat(markerElement.getAttribute('lng')));

    var infowincontent = document.createElement('div');
    var strong = document.createElement('strong');
    strong.textContent = name
    infowincontent.appendChild(strong);
    infowincontent.appendChild(document.createElement('br'));

    var text = document.createElement('text');
    text.textContent = address
    infowincontent.appendChild(text);
    //var icon = customLabel[type] || {};
    
    var icon =  {};
    
    var marker = new google.maps.Marker({
      map: map,
      position: point,
      label: icon.label
    });

    marker.addListener('click', function() {
      infoWindow.setContent(infowincontent);
      infoWindow.open(map, marker);
    });

  });
}